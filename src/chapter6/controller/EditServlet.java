package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		int id = Integer.parseInt(request.getParameter("textId"));
		Message message = new MessageService().select(id);//Textが返ってきてる。

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);//URLが保持される（情報が保持される）
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String text = request.getParameter("newText");
		int id = Integer.parseInt(request.getParameter("textId"));

		new MessageService().update(text, id);

		request.getRequestDispatcher("./index.jsp").forward(request, response);
	}
}