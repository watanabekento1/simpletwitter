package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	//doGetはURL・doPostは入力情報をServiceへ転送。
	@Override

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request); //下から情報を定義されたuserが返ってくる。
		if (!isValid(user, errorMessages)) {//なんかエラーが入ってた場合
			request.setAttribute("errorMessages", errorMessages); //[,]の左右のデータを結び付けて下のjspに向かうようにするメソッド。
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("./");
	}

	//以下でユーザー情報をuserに定義する（セットする）
	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		//JSPからの以下の情報requestに対し以下を適用。
		user.setName(request.getParameter("name")); //UserDaoで入れたvalueの取得。
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		user.setDescription(request.getParameter("description"));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {//35行目のList
		/*もし以下の条件のどれかにひっかかったらエラーメッセージがListに格納するメソッド。
		何かしら入ってたらfalse，そうでなければtrueを返す処理*/
		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String email = user.getEmail();

		if (!StringUtils.isEmpty(name) && (20 < name.length())) { //isEmpty(引数)で引数に実引数が入ってるかbooleanで返す。
			errorMessages.add("名前は20文字以下で入力してください");
		}

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if (!StringUtils.isEmpty(email) && (50 < email.length())) {
			errorMessages.add("メールアドレスは50文字以下で入力してください");
		}
		if (new UserService().select(user.getAccount()) !=null ) {//
			errorMessages.add("アカウント名が重複しています");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}